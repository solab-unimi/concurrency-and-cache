CFLAGS=-DN=$(shell getconf _NPROCESSORS_CONF || echo 2) \
       -DCACHE_LINESIZE=$(shell getconf LEVEL1_DCACHE_LINESIZE || echo 64)
PROGS=sums sumc sumc2 sumc3
TIMEPROG=time -p

all: clean bench

sums: sums.c
	gcc ${CFLAGS} -o $@ $<

sumc: sumc.c
	gcc $(CFLAGS) -pthread -o $@ $<

sumc2: sumc2.c
	gcc $(CFLAGS) -pthread -o $@ $<

sumc3: sumc3.c
	gcc $(CFLAGS) -pthread -o $@ $<

bench: ${PROGS}
	for p in ${PROGS}; do ${TIMEPROG} ./$$p; done

clean:
	@rm -f ${PROGS}

.PHONY: all clean bench
