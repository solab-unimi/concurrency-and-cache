# Processor caches cannot be ignored in concurrent programs 



## Go

Original article (and code) is here:
https://appliedgo.net/concurrencyslower/

On my system with 8 processors:

```sh
cd concurrencyslower/
go test -bench .
```
```
goos: linux
goarch: amd64
BenchmarkSerialSum-8                   5         307832033 ns/op
BenchmarkConcurrentSum-8               2         802125438 ns/op
BenchmarkMyConcurrentSum-8            20          81753325 ns/op
BenchmarkChannelSum-8                 20          84626895 ns/op
PASS
```

## C

```sh
make
```
```
gcc -DN=8 -DCACHE_LINESIZE=64 -o sums sums.c
gcc -DN=8 -DCACHE_LINESIZE=64 -pthread -o sumc sumc.c
gcc -DN=8 -DCACHE_LINESIZE=64 -pthread -o sumc2 sumc2.c
gcc -DN=8 -DCACHE_LINESIZE=64 -pthread -o sumc3 sumc3.c
for p in sums sumc sumc2 sumc3; do time -p ./$p; done
Sequential: 499999999500000000
real 2.28
user 2.28
sys 0.00
ConcurrentSum: 499999999500000000
real 2.68
user 20.18
sys 0.00
MyConcurrentSum: 499999999500000000
real 0.48
user 3.79
sys 0.00
ConcurrentSumUncached: 499999999500000000
real 0.47
user 3.68
sys 0.00
```
