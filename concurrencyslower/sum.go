package concurrencyslower

import (
	"runtime"
	"sync"
)

const (
	limit = 1000000000
)

// SerialSum sums up all numbers from 0 to limit, sice and easy!

func SerialSum() int {
	sum := 0
	for i := 0; i < limit; i++ {
		sum += i
	}
	return sum
}

func ConcurrentSum() int {

	// Get the number of available logical cores. Usually this is 2*c where
	// c is the number of physical cores and 2 is the number of hyperthreads
	// per core.

	n := runtime.GOMAXPROCS(0)

	// We need to collect the results from the n goroutines somewhere. How
	// about a gloaal slice with one element for every goroutine.

	sums := make([]int, n)

	// Now we can spawn the goroutines. A WaitGroup helps us detecting when
	// all goroutines have finished.

	wg := sync.WaitGroup{}
	for i := 0; i < n; i++ {

		// One Add() for each goroutine spawned.

		wg.Add(1)
		go func(i int) {

			// Split the “input” into n chunks that do not overlap.

			start := (limit / n) * i
			end := start + (limit / n)

			// Run the loop over the given chunk.

			for j := start; j < end; j += 1 {
				sums[i] += j
			}

			// Tell the WaitGroup that this goroutine has completet its task.

			wg.Done()
		}(i)
	}

	// Wait for all goroutines to call Done().

	wg.Wait()

	// Collect the total sum from the sums of the n chunks.

	sum := 0
	for _, s := range sums {
		sum += s
	}
	return sum
}

func MyConcurrentSum() int {

	// Get the number of available logical cores. Usually this is 2*c where
	// c is the number of physical cores and 2 is the number of hyperthreads
	// per core.

	n := runtime.GOMAXPROCS(0)

	// We need to collect the results from the n goroutines somewhere. How
	// about a gloaal slice with one element for every goroutine.

	sums := make([]int, n)

	// Now we can spawn the goroutines. A WaitGroup helps us detecting when
	// all goroutines have finished.

	wg := sync.WaitGroup{}
	for i := 0; i < n; i++ {

		// One Add() for each goroutine spawned.

		wg.Add(1)
		go func(i int) {

			// Split the “input” into n chunks that do not overlap.

			start := (limit / n) * i
			end := start + (limit / n)

			// Run the loop over the given chunk.
			s := 0
			for j := start; j < end; j += 1 {
				s += j
			}

			// Tell the WaitGroup that this goroutine has completet its task.
			sums[i] = s
			wg.Done()
		}(i)
	}

	// Wait for all goroutines to call Done().

	wg.Wait()

	// Collect the total sum from the sums of the n chunks.

	sum := 0
	for _, s := range sums {
		sum += s
	}
	return sum
}

func ChannelSum() int {
	n := runtime.GOMAXPROCS(0)

	// A channel of ints will collect all intermediate sums.

	res := make(chan int)

	for i := 0; i < n; i++ {

		// The goroutine now receives a second parameter, the result
		// channel. The arrow pointing “into” the chan keyword turns
		// this channel into a send-only channel inside this function.

		go func(i int, r chan<- int) {

			// This local variable replaces the global slice.

			sum := 0

			// As before, we divide the input into n chunks of equal size.

			start := (limit / n) * i
			end := start + (limit / n)

			// Calculate the intermediate sum.

			for j := start; j < end; j += 1 {
				sum += j
			}

			// Pass the final sum into the channel.

			r <- sum

			// Call the goroutine and pass the CPU index and the channel.

		}(i, res)
	}

	sum := 0

	// This loop reads n values from the channel. We know exactly how many
	// elements we will receive through the channel, hence we need no

	for i := 0; i < n; i++ {

		// Read a value from the channel and add it to sum.

		// The channel blocks when there are no elements to read. This
		// provides a “natural” synchronization mechanism. The loop must
		// wait until there is an element to read, and does not finish
		// before all n elements have been passed through the channel.

		sum += <-res
	}
	return sum
}
