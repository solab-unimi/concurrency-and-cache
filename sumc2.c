#include <pthread.h>

#define LIMIT 1000000000

int printf(const char*, ...);

long sums[N];
int done = 0;

pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;



void* run(void* param){
  int i = *((int*)(param));
  long start = (LIMIT / N) * i;
  long end = start + (LIMIT / N);
  long j;
  long s = 0;
  for (j=start; j<end; j++){
    s += j;
  }
  sums[i] = s;
  pthread_mutex_lock(&mut);
  done += 1;
  if (done == N) pthread_cond_signal(&cond);
  pthread_mutex_unlock(&mut);
  return 0;
}


int main(){
  pthread_t p[N];
  int ids[N];
  int i;
  long sum = 0;
  for (i=0; i<N; i++){
    ids[i] = i;
    pthread_create(&p[i], NULL, run, &ids[i]);
  }
  pthread_mutex_lock(&mut);
  while (done < N)
    pthread_cond_wait(&cond, &mut);
  pthread_mutex_unlock(&mut);
  for (i=0; i<N; i++){
    sum += sums[i];
  }
  printf("MyConcurrentSum: %ld\n", sum);
  return 0;
}
