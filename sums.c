#define LIMIT 1000000000
int printf(const char*, ...);


int main(){
  long i;
  long sum = 0;
  for (i=0; i<LIMIT; i++){
    sum += i;
  }
  printf("Sequential: %ld\n", sum);
  return 0;
}
